// import dependencies
require('dotenv').config()
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const UserRoutes = require('./routes/user');

// Step : 1 - Init App With Express
const app = express();

// Step : 2 - Start Server
const _PORT = process.env.PORT
app.listen(_PORT, () => {
    console.log('Server Started');
})

//Step : 3 - Database Connection
mongoose.connect(process.env.DATABASE, { useNewUrlParser: true });
mongoose.connection.on('connected', () => {
    console.log('DataBase Connected');
})

mongoose.connection.on('error', (err) => {
    console.log('Can Not Connect To The Database ' + err);
})

app.use(bodyParser.json());

//Using The Routes [Routes Should Be Written After The BodyParser Ussage]
app.use('/users', UserRoutes)