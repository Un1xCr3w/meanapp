const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const UserSchema = mongoose.Schema({
    name: String,
    email: { type: String, required: true },
    password: { type: String, required: true }
})

UserSchema.pre('save', (next) => {
    //Generate Salrt
    bcrypt.genSalt(10, (error, salt) => {

        if (error) {
            return next(erro)
        }
        bcrypt.hash(this.password, salt, (err, hashedPassword) => {
            if (error) {
                return next(erro)
            }
            this.password = hashedPassword
        });
    });
})
const User = mongoose.model('User', UserSchema)
module.exports = User;