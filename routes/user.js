const express = require('express');
const router = express.Router();
const User = require('../models/user');

//Login Route
router.get('/auth', (req, res, next) => {
    res.send('Login')
})

//Registeration Route
router.post('/register', (req, res, next) => {
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    });
    newUser.save((error, user) => {

        res.send({
            success: true,
            message: 'User Saved!',
            user
        })
    });
    console.log(newUser);

})

module.exports = router;